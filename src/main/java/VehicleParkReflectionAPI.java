import exceptions.NoVehiclesLeftException;
import exceptions.WrongSortingParametersException;
import utils.Printer;
import vehicles.ElectricBus;
import vehicles.GasBus;
import vehicles.OrdinaryBus;
import vehicles.Vehicle;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

public class VehicleParkReflectionAPI {
    private List<Vehicle> allVehicles;
    private long overallVehicleCost;

    public static void main(String[] args) {
        try {
            Class<VehicleParkReflectionAPI> park = VehicleParkReflectionAPI.class;
            VehicleParkReflectionAPI parkObject = park.newInstance();
            Method createVehicles = park.getDeclaredMethod("createVehicles", int.class);
            createVehicles.invoke(parkObject, new Random().nextInt(10) + 5);

            Method countOverallCost = park.getDeclaredMethod("countOverallCost");
            countOverallCost.invoke(parkObject);

            Method printAllVehicles = park.getDeclaredMethod("printAllVehicles");
            Method printVehicles = park.getDeclaredMethod("printVehicles", List.class);
            Method searchVehiclesBy = park.getDeclaredMethod("searchVehiclesBy", int.class, int.class, int.class,
                    int.class, int.class, int.class);

            printAllVehicles.invoke(parkObject);
            List<Vehicle> vehicles = (List<Vehicle>) searchVehiclesBy.invoke(parkObject,8000, 17500, 50, 75,
                    50, 80);
            printVehicles.invoke(parkObject, vehicles);

            Method printMetadata = park.getMethod("printMetadata");
            printMetadata.invoke(parkObject);
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void printMetadata() throws Exception {
        Class<?> park = this.getClass();
        System.out.println("Metadata of " + park.getName() + " :");
        Field[] fields = park.getDeclaredFields();
        Printer.printFieldsMetadata(this, fields);

        Method[] methods = park.getDeclaredMethods();
        Printer.printDeclaredMethods(methods);

        Constructor<?>[] constructors = park.getDeclaredConstructors();
        Printer.printConstructors(constructors);
    }

    private void printAllVehicles() {
        System.out.println("All the vehicles of the park:");
        for (int i = 1; i <= allVehicles.size(); i++) {
            System.out.println(i + ") " + allVehicles.get(i - 1));
        }
        System.out.println("\n======================\n");
    }

    private void printVehicles(List<Vehicle> vehicles) {
        System.out.println("Vehicles satisfying the parameters:");
        for (int i = 1; i <= vehicles.size(); i++) {
            System.out.println(i + ") " + vehicles.get(i - 1));
        }
        System.out.println("\n======================\n");
    }

    private void createVehicles(int amount) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        allVehicles = ArrayList.class.getConstructor().newInstance();
        Random random = Random.class.getConstructor().newInstance();

        for (int i = 0; i < amount; i++) {
            int mass = 5000 + random.nextInt(25000);
            int maxSpeed = 45 + random.nextInt(50);
            int capacity = 30 + random.nextInt(100);
            int typeOrdinal = random.nextInt(3);
            Class<?> clazz = null;
            switch (typeOrdinal) {
                case 0: {
                    clazz = OrdinaryBus.class;
                    break;
                }
                case 1: {
                    clazz = ElectricBus.class;
                    break;
                }
                case 2: {
                    clazz = GasBus.class;
                    break;
                }
            }
            allVehicles.add((Vehicle) clazz.getConstructor(int.class, int.class, int.class).newInstance(mass, maxSpeed, capacity));
        }

        Collections.sort(allVehicles);
    }

    private void countOverallCost() {
        overallVehicleCost = allVehicles.stream()
                .map(Vehicle::getCost)
                .reduce(0L, Long::sum);
    }

    public List<Vehicle> getAllVehicles() {
        return allVehicles;
    }

    public long getOverallVehicleCost() {
        return overallVehicleCost;
    }

    public List<Vehicle> searchVehiclesBy(int fromMass, int toMass, int fromMaxSpeed, int toMaxSpeed,
                                          int fromCapacity, int toCapacity) {
        if (toMass < fromMass || toMaxSpeed < fromMaxSpeed || toCapacity < fromCapacity) {
            throw new WrongSortingParametersException("You have entered wrong parameters. " +
                    "Right border can't be more than left one.");
        }

        List<Vehicle> vehicles = allVehicles.stream()
                .filter(v -> v.getMass() >= fromMass && v.getMass() <= toMass)
                .filter(v -> v.getMaxSpeed() >= fromMaxSpeed && v.getMaxSpeed() <= toMaxSpeed)
                .filter(v -> v.getCapacity() >= fromCapacity && v.getMaxSpeed() <= toCapacity)
                .collect(Collectors.toList());

        if (vehicles.size() == 0) {
            throw new NoVehiclesLeftException("There are no vehicles according to the sorting parameters");
        } else {
            return vehicles;
        }
    }
}
