package utils;

import vehicles.Vehicle;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.List;

public class Printer {
    public static void printConstructors(Constructor<?>[] constructors) {
        System.out.println(" Constructors: {");
        for (Constructor<?> constructor : constructors) {
            System.out.println("  => " + constructor);
        }
        System.out.println("  }");
    }

    public static void printDeclaredMethods(Method[] methods) {
        if (methods.length > 1) {
            System.out.println("  Methods: {");
        }
        for (Method method : methods) {
            if (!method.isSynthetic()) {
                StringBuilder sb = new StringBuilder("  => ");
                sb.append(Modifier.toString(method.getModifiers()));
                sb.append(" ");
                sb.append(method.getReturnType().getSimpleName());
                sb.append(" ");
                sb.append(method.getName());
                sb.append("(");

                Parameter[] parameters = method.getParameters();
                for (int i = 0; i < parameters.length; i++) {
                    sb.append(parameters[i].getType().getSimpleName());
                    if (i != parameters.length - 1) {
                        sb.append(", ");
                    }
                }
                sb.append(")");

                Class<?>[] exceptions = method.getExceptionTypes();
                for (int i = 0; i < exceptions.length; i++) {
                    if (i == 0) {
                        sb.append(" throws ").append(exceptions[i].getSimpleName());
                    } else {
                        sb.append(", ").append(exceptions[i].getSimpleName());
                    }
                }

                System.out.println(sb.toString());
            }
        }
        if (methods.length > 1) {
            System.out.println("  }");
        }
    }

    public static void printFieldsMetadata(Object obj, Field[] fields) throws Exception {
        if (fields.length > 1) {
            System.out.println("  Fields: {");
        }
        for (Field field : fields) {
            if (!field.canAccess(obj)) {
                field.setAccessible(true);
            }
            String name = field.getName();
            if (field.getType().isPrimitive()) {
                if (field.getType() == long.class) {
                    System.out.println("  => " + name + " = " + field.getLong(obj));
                } else if (field.getType() == int.class) {
                    System.out.println("  => " + name + " = " + field.getInt(obj));
                }
            } else if (field.getType() == List.class) {
                List<Vehicle> list = (List<Vehicle>) field.get(obj);
                System.out.println("  => " + name + ": {");
                for (Vehicle vehicle : list) {
                    Method printMetadata = vehicle.getClass().getMethod("printMetadata");
                    printMetadata.invoke(vehicle);
                }
                System.out.println("  }");
            }
        }
        if (fields.length > 1) {
            System.out.println("  }");
        }
    }

    public static Field[] getAllFields(Class<?> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        if (clazz.getSuperclass() != Object.class) {
            Field[] superClassFields = clazz.getSuperclass().getDeclaredFields();
            Field[] result = Arrays.copyOf(fields, fields.length + superClassFields.length);
            System.arraycopy(superClassFields, 0, result, fields.length, superClassFields.length);
            return result;
        }
        return fields;
    }

    public static Method[] getAllMethods(Class<?> clazz) {
        Method[] methods = clazz.getDeclaredMethods();
        if (clazz.getSuperclass() != Object.class) {
            Method[] superClassMethods = clazz.getSuperclass().getDeclaredMethods();
            Method[] result = Arrays.copyOf(methods, methods.length + superClassMethods.length);
            System.arraycopy(superClassMethods, 0, result, methods.length, superClassMethods.length);
            return result;
        }
        return methods;
    }
}
