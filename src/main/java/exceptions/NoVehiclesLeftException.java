package exceptions;

public final class NoVehiclesLeftException extends VehicleException {
    public NoVehiclesLeftException(String message) {
        super(message);
    }
}
