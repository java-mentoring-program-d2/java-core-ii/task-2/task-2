package vehicles;

public class OrdinaryBus extends Vehicle{

    public OrdinaryBus(int mass, int maxSpeed, int capacity) {
        super(mass, maxSpeed, capacity);
    }

    @Override
    EnergyType getType() {
        return EnergyType.GASOLINE;
    }

}
