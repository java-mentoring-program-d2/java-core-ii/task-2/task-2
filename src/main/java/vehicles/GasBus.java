package vehicles;

public class GasBus extends Vehicle {

    public GasBus(int mass, int maxSpeed, int capacity) {
        super(mass, maxSpeed, capacity);
    }

    @Override
    EnergyType getType() {
        return EnergyType.GAS;
    }

}
