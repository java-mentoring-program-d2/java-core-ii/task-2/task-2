package vehicles;

public class ElectricBus extends Vehicle {
     public ElectricBus(int mass, int maxSpeed, int capacity) {
        super(mass, maxSpeed, capacity);
    }

    @Override
    EnergyType getType() {
        return EnergyType.ELECTRICITY;
    }
}
